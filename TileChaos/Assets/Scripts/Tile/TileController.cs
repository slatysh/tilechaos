using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TileController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Image _img;
    [SerializeField] private TextMeshProUGUI _indTxt;
    [SerializeField] private TextMeshProUGUI _clickCountTxt;

    [HideInInspector] public GameController GameControllerInst;
    [HideInInspector] public int Row;
    [HideInInspector] public int Column;
    [HideInInspector] public int Ind;

    private int _clickCount = 0;

    public void Start()
    {
        Ind = Row * GameControllerInst.size + Column + 1;
        _indTxt.text = Ind.ToString();
        _clickCountTxt.text = _clickCount.ToString();
        Colorize();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ClickHandle();
        if (_clickCount % GameControllerInst.size == 0)
        {
            GameControllerInst.ClickHandleOtherTiles(Ind);
        }
    }

    public void ClickHandle()
    {
        _clickCount++;
        _clickCountTxt.text = _clickCount.ToString();
        Colorize();
    }

    private void Colorize()
    {
        bool odd = _clickCount % 2 == 0;
        _img.color = (odd) ? Color.white : Color.red;
        _indTxt.color = (odd) ? Color.black : Color.white;
    } 
   
}
