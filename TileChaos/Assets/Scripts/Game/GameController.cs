using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int size = 0;

    [SerializeField] private Slider _sizeSlider;
    [SerializeField] private Transform _tilePrefab;
    [SerializeField] private GridLayoutGroup _grid;
    private RectTransform _gridRectTransform;
    private List<TileController> _tileControllerArr = new List<TileController>();    
    
    void Start()
    {
        _gridRectTransform = _grid.GetComponent<RectTransform>();
        size = (int)_sizeSlider.value;
        _sizeSlider.onValueChanged.AddListener((v) =>
        {
            size = (int)v;
            ResetGame();
        });
        StartGame();
    }
        
    void Update()
    {
    }

    public void ResetGame()
    {
        _tileControllerArr.ForEach(t =>
        {
            Destroy(t.gameObject);
        });
        _tileControllerArr.Clear();
        StartGame();
    }

    public void StartGame()
    {
        _grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        _grid.constraintCount = size;
        float additionalH = (size - 1) * _grid.spacing.x + _grid.padding.left + _grid.padding.right;
        float additionalV = (size - 1) * _grid.spacing.y + _grid.padding.top + _grid.padding.bottom;
        int cellSize = Mathf.FloorToInt((Mathf.Min(_gridRectTransform.rect.width - additionalH, _gridRectTransform.rect.height - additionalV)) / size);
        _grid.cellSize = new Vector2(cellSize, cellSize);        
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                Transform tile = Instantiate(_tilePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                TileController tileController = tile.GetComponent<TileController>();
                tileController.GameControllerInst = this;
                tileController.Row = i;
                tileController.Column = j;
                tile.SetParent(_grid.transform);
                _tileControllerArr.Add(tileController);
            }
        }
    }

    public void ClickHandleOtherTiles(int initedInd)
    {
        _tileControllerArr.ForEach(t =>
        {
            if (t.Ind != initedInd)
            {
                t.ClickHandle();
            }
        });
    }
}

