using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderController : MonoBehaviour
{
    [SerializeField] private string _title = "";
    [SerializeField] private Slider _slider;
    [SerializeField] private TextMeshProUGUI _sliderVal;

    void Start()
    {
        _slider.onValueChanged.AddListener((v) =>
        {
            CommitValue(v);
        });
        CommitValue(_slider.value);
    }

    private void CommitValue(float val)
    {
        _sliderVal.text = _title + val.ToString("0");
    }

}
